#!/usr/bin/env python

import sys
import requests
import random
import time
import json
import getopt

"""
checkUrlList.py

SYNOPSIS

     checkUrlList.py [ -qh ] [ -d debugFile ] [ -i urlFile ] [ -o outputFile ]

OPTIONS
       -h        : This help screen
       -q        : Quiet printing of progress to the terminal
       -d <file> : Send debugging output to <file>
       -o <file> : Send results to <file> instead of stdout
       -i <file> : Read URLs from <file> instead of stdin

This script is intended to be used as a testing tool at Windsor Circle to check the
validity of client's image URLs in their product data.  It will load a set of URLs and
report information about the HTTP response codes returned as well as the content type.

It uses a random 2 to 8 second wait between requests in order to attempt to avoid
triggering and anti-DDOS security on the client's website.
"""

def createUrlList(File):
	"""
	Reads in the list of URLs to be processed from stdin and puts them into a list
	after having removed duplicates.
	
	Returns the deduped list of URLs.
	"""
	urlList = []
	
	# Read a line at a time from stdin and add the item to the urlList list
	for imageUrl in File.readlines():
		imageUrl = imageUrl.strip()
		urlList.append(imageUrl)
		
	# Convert the list to a set, thereby eliminating duplicates and then back to a list
	#  again before passing it back to the caller
	return list(set(urlList))

def runStats(urlList, quiet):
	"""
	Takes a list object of URLs as a single parameter.
	Uses the requests Python module for making the HTTP requests.
	Returns a dictionary with stats from all the queries.
	"""
	
	stats = {}
	count = 1
	
	# Initialize the randomizer
	random.random()

	for imageUrl in urlList:
		# These are all the exceptions that we want to capture and abort if we see them.
		try:
			# This is the call that loads the URL
			response = requests.get( imageUrl.rstrip() )
		except requests.exceptions.ConnectionError:
			print >> sys.stderr, "There was an error connecting to the server."
			print >> sys.stderr, "URL was: %s", imageUrl
			sys.exit(-1)
		except requests.exceptions.RequestException:
			print >> sys.stderr, "There was an ambiguous exception that occurred while loading the image."
			print >> sys.stderr, "URL was: %s", imageUrl
		except requests.exceptions.URLRequired:
			print >> sys.stderr, "Looks like the URL was malformed."
			print >> sys.stderr, "URL was: %s", imageUrl
		except requests.exceptions.ConnectTimeout:
			print >> sys.stderr, "Timeout connecting to the server."
			print >> sys.stderr, "URL was: %s", imageUrl
		except requests.exceptions.ReadTimeout:
			print >> sys.stderr, "Timeout connecting to the server."
			print >> sys.stderr, "URL was: %s", imageUrl
		except requests.exceptions.Timeout:
			print >> sys.stderr, "Timeout connecting to the server."
			print >> sys.stderr, "URL was: %s", imageUrl
		
		if not quiet:
			print count, ": ", imageUrl
		
		# If we've not seen this return code before, we need to enter it into the dictionary.
		if response.status_code not in stats:
			stats[response.status_code] = {}
			stats[response.status_code]['count'] = 0
			stats[response.status_code]['urls'] = []
			stats[response.status_code]['redirect_urls'] = []
			stats[response.status_code]['redirects'] = 0
			stats[response.status_code]['non_images'] = 0
			stats[response.status_code]['name'] = ''
		# If the requests object has history information, that means that there were redirects.  So, log the
		#	URLs as redirects.  Note: The response code will be that of the last link that was redirected, not
		#	a redirect response code.  That's contained in the history object, but we're not using that right
		#	now.
		if response.history:
			stats[response.status_code]['redirects'] += 1
			stats[response.status_code]['redirect_urls'].append([imageUrl, response.headers.get('content-type')])
		else:
			stats[response.status_code]['count'] += 1
			stats[response.status_code]['urls'].append([imageUrl, response.headers.get('content-type')])
		
		# If the content type is not an image
		if response.headers.get('content-type')[:5] != 'image':
			stats[response.status_code]['non_images'] += 1
			
		stats[response.status_code]['name'] = response.reason
		# Need to make sure that we're not loading URLs back-to-back as we might trigger protections on the web server
		#  for DDOS attacks.  So, we wait 2 to 8 seconds between requests
		waitTime = random.randint( 2,8 )
		time.sleep( waitTime )
		
		count += 1
		
	return( stats )

def printStats(finalStats, File, printUrls = False):
	"""
	Prints the output in a human-readable format

	Takes two parameters:
	1) finalStats: The dictionary that contains the stats gathered during the runStats() function.
	2) printUrls (optional ): Tells the function to print the URLs for each of the return codes.  The
		default behavior is to not print them.

	Returns nothing.
	"""
	
	# Iterate through the dictionary
	for code, results in finalStats.iteritems():
		File.write( "\n" )
		File.write( "HTTP Response Code" + str(code) + "\n" )
		File.write( "  Total # nonredirects:" + str(results['count']) + "\n" )
		File.write( "  Total # of redirects:" + str(results['redirects']) + "\n" )
		File.write( "  Total # of responses:" + str((results['count'] + results['redirects'])) + "\n" )
		if printUrls:
			File.write( "  URLs that did NOT redirect:\n" )
			# Cycle through and print the URLs
			for url in results['urls']:
				File.write( "    " + str(url) + "\n" )
			File.write( "  URLs that DID redirect:\n" )
			# Cycle through and print the URLs from those that redirected
			for redirectUrl in results['redirect_urls']:
				File.write( "    " + redirectUrl + "\n" )

def main():

	debugFile = False
	outFile = False
	quiet = False
	inFile = sys.stdin
	outFile = sys.stdout
	
	# Get all of the command line options
	try:
		opts, args = getopt.getopt(sys.argv[1:], "d:hi:o:q")
	except getopt.GetoptError, err:
		print str(err)
		sys.exit(1)

	for o, a in opts:
		if o == "-d":
			debugFile = open(a, "wt")
		elif o == "-h":
			print ""
			print "SYNOPSIS"
			print ""
			print "     checkUrlList.py [ -qh ] [ -d debugFile ] [ -i urlFile ] [ -o outputFile ]"
			print ""
			print "OPTIONS"
			print "       -h        : This help screen"
			print "       -q        : Quiet printing of progress to the terminal"
			print "       -d <file> : Send debugging output to <file>"
			print "       -o <file> : Send results to <file> instead of stdout"
			print "       -i <file> : Read URLs from <file> instead of stdin"
			print ""
			sys.exit(0) 
		elif o == "-o":
			outFile = open(a, "wt")
		elif o == "-i":
			inFile = open(a, "rt")
		elif o == "-q":
			quiet = True
		else:
			print "Illegal option: " + o
			sys.exit(-1)

	URLs = createUrlList(inFile)
	statistics = runStats(URLs, quiet)
	printStats(statistics, outFile, printUrls=True)

	if debugFile:
		# Dump the raw stats dictionary as JSON for debugging.	
		debugFile.write(json.dumps(statistics, sort_keys=True, indent=2 ) )
		debugFile.close()

	outFile.close()

if __name__ == "__main__":
    main()
